package com.company;


import sun.security.provider.certpath.Vertex;

import java.io.IOException;
import java.util.*;

/**
 * Created by negrita91 on 16/04/15.
 */
public class Graf {

    private double[][] g;

//    public void outputGDF(String fileName)
//    {
//        HashMap<Vertex, String> idToName = new HashMap<Vertex, String>();
//        try {
//            FileWriter out = new FileWriter(fileName);
//            int count = 0;
//            out.write("nodedef> name,label,style,distance INTEGER\n");
//            // write vertices
//            for (Vertex v: myVertices.values())
//            {
//                String id = "v"+ count++;
//                idToName.put(v, id);
//                out.write(id + "," + escapedVersion(v.name));
//                out.write(",6,"+v.distance+"\n");
//            }
//            out.write("edgedef> node1,node2,color\n");
//            // write edges
//            for (Vertex v : myVertices.values())
//                for (Vertex w : myAdjList.get(v))
//                    if (v.compareTo(w) < 0)
//                    {
//                        out.write(idToName.get(v)+","+
//                                idToName.get(w) + ",");
//                        if (v.predecessor == w ||
//                                w.predecessor == v)
//                            out.write("blue");
//                        else
//                            out.write("gray");
//                        out.write("\n");
//                    }
//            out.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }









//    private double[][] grafo;
//    private int vertices;
//    public Graf(){
//    }
//    public Graf Graf(Graf G) {
//        Graf Nou;
//        Nou = G;
//        return Nou;
//    }
//    public Integer size(){
//       return vertices;
//    }
//
//    public HashSet<Integer> getNodes() {
//        HashSet<Integer> nodes;
//    }
//
//    addNode(Integer id): Boolean
//
//    removeNode(Integer id): Boolean
//
//    existeixNode(Integer id): Boolean
//
//    addAresta(Integer a, Integer, Double Pes): Boolean
//
//    removeAresta(Integer a, Integer b);
//
//    existeixAresta(Integer a, Integer b): Boolean
//
//    setPes(Integer a, Integer b, Double Pes): Boolean
//
//    getPes(Integer a, Integer b): Double
//
//    getAdjacents(String id): HashSet<Integer>

    public void crearGraf(){
        g = new double[10][10];
        for(int i=0; i<10; ++i){
            for(int j=0; j<10; ++j){
                g[i][j]=0.0;
            }
        }
    }

    public boolean addAresta(Integer a, Integer b, Double Pes){
        g[a][b] = Pes;
        g[b][a] = Pes;
        return true;
    }

    public Boolean isNeighbor(Integer found, Integer candidate){
        return (g[found][candidate]>0);
    }

    public Collection<Integer> getVertices(){
        Collection<Integer> v = new ArrayList<Integer>();
        for (int i=0; i<g.length; ++i)
            v.add(i);
        return v;
    }
}
