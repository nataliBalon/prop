package com.company;

import sun.security.provider.certpath.Vertex;

import java.time.temporal.Temporal;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code her

        Graf G = new Graf();
        G.crearGraf();

        G.addAresta(0, 1, 1.0);
        G.addAresta(0, 2, 1.0);
        G.addAresta(0, 3, 1.0);

        G.addAresta(1, 2, 1.0);
        G.addAresta(1, 3, 1.0);
        G.addAresta(1, 4, 1.0);
        G.addAresta(1, 5, 1.0);

        G.addAresta(2, 3, 1.0);
        G.addAresta(2, 7, 1.0);
        G.addAresta(2, 8, 1.0);

        G.addAresta(3, 4, 1.0);
        G.addAresta(3, 6, 1.0);
        G.addAresta(3, 7, 1.0);
        G.addAresta(3, 8, 1.0);
        G.addAresta(3, 9, 1.0);

        G.addAresta(4, 5, 1.0);
        G.addAresta(4, 6, 1.0);
        G.addAresta(4, 7, 1.0);
        G.addAresta(4, 8, 1.0);
        G.addAresta(4, 9, 1.0);

        G.addAresta(6, 7, 1.0);
        G.addAresta(6, 8, 1.0);
        G.addAresta(6, 9, 1.0);

        G.addAresta(7, 8, 1.0);




        CliquePercolation finder = new CliquePercolation(G);

        HashSet<HashSet<Integer>> comunidades = finder.executa(G, 1); //me da todas las comunidades del grafo formado por cliques
        //Enseñar las comunidades
        int q = 1;
        for(HashSet<Integer> obj : comunidades){

            System.out.println("comunidad " + q);
            for(Integer o : obj){
                System.out.println(" " + o);
            }
            ++q;
        }


    }
}
