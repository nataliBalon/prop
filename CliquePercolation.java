package com.company;



import java.util.*;

/**
 * Created by negrita91 on 13/04/15.
 */
public class CliquePercolation {
//    public void executa( g, int perc){
//        asignarGrafo(g, perc);
//    }

//    public boolean isConnected(int i, int j){
//        return (grafo[i][j]!=1);
//    }

//    public HashSet<HashSet<Integer>> findCliques(int n) {
//        // find all cliques in the graph g of size n.
//        // Hint: use recursion
//    }
//
//    public int[] findCommunities(){
//        // find all communities using clique perlocation.
//        // result should be an array where each vertex has community ID
//        // two vertices are from the same community if communities[i]==communities[j]
//    }

    //~ Instance fields --------------------------------------------------------

    private final Graf graph;

    private HashSet<HashSet<Integer>> cliques;

    //~ Constructors -----------------------------------------------------------


    /**
     * Creates a new clique finder. Make sure this is a simple graph.
     *
     * @param graph the graph in which cliques are to be found; graph must be
     * simple
     */
    public CliquePercolation(Graf graph)
    {

        this.graph = graph;
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * Finds all maximal cliques of the graph. A clique is maximal if it is
     * impossible to enlarge it by adding another vertex from the graph. Note
     * that a maximal clique is not necessarily the biggest clique in the graph.
     *
     * @return Collection of cliques (each of which is represented as a Set of
     * vertices)
     */
    private HashSet<HashSet<Integer>> getAllMaximalCliques()
    {
        // TODO:  assert that graph is simple

        cliques = new HashSet<HashSet<Integer>>();
        List<Integer> potential_clique = new ArrayList<Integer>();
        List<Integer> candidates = new ArrayList<Integer>();
        List<Integer> already_found = new ArrayList<Integer>();

        candidates.addAll(graph.getVertices());
        findCliques(potential_clique, candidates, already_found);
        return cliques;
    }

    /**
     * Finds the biggest maximal cliques of the graph.
     *
     * @return Collection of cliques (each of which is represented as a Set of
     * vertices)
     */
    private HashSet<HashSet<Integer>> getBiggestMaximalCliques()
    {
        // first, find all cliques
        getAllMaximalCliques();

        int maximum = 0;
        HashSet<HashSet<Integer>> biggest_cliques = new HashSet<HashSet<Integer>>();
        for (HashSet<Integer> clique : cliques) {
            if (maximum < clique.size()) {
                maximum = clique.size();
            }
        }
        for (HashSet<Integer> clique : cliques) {
            if (maximum == clique.size()) {
                biggest_cliques.add(clique);
            }
        }
        return biggest_cliques;
    }

    private void findCliques(
            List<Integer> potential_clique,
            List<Integer> candidates,
            List<Integer> already_found)
    {
        List<Integer> candidates_array = new ArrayList<Integer>(candidates);
        if (!end(candidates, already_found)) {
            // for each candidate_node in candidates do

            for(int i=0; i<candidates_array.size(); i++) {
                Integer candidate = candidates_array.get(i);
                List<Integer> new_candidates = new ArrayList<Integer>();
                List<Integer> new_already_found = new ArrayList<Integer>();

                // move candidate node to potential_clique
                potential_clique.add(candidate);
                candidates.remove(candidate);

                // create new_candidates by removing nodes in candidates not
                // connected to candidate node
                for(int j=0; j<candidates.size(); j++){
                    Integer new_candidate = candidates.get(j);
                    if (graph.isNeighbor(candidate, new_candidate))
                    {
                        new_candidates.add(new_candidate);
                    } // of if
                } // of for

                // create new_already_found by removing nodes in already_found
                // not connected to candidate node
                for(int k=0; k<already_found.size(); k++){
                    Integer new_found = already_found.get(k);
                    if (graph.isNeighbor(candidate, new_found)) {
                        new_already_found.add(new_found);
                    } // of if
                } // of for

                // if new_candidates and new_already_found are empty
                if (new_candidates.isEmpty() && new_already_found.isEmpty()) {
                    // potential_clique is maximal_clique
                    cliques.add(new HashSet<Integer>(potential_clique));
                } // of if
                else {
                    // recursive call
                    findCliques(
                            potential_clique,
                            new_candidates,
                            new_already_found);
                } // of else

                // move candidate_node from potential_clique to already_found;
                already_found.add(candidate);
                potential_clique.remove(candidate);
            } // of for
        } // of if
    }

    private boolean end(List<Integer> candidates, List<Integer> already_found)
    {
        // if a node in already_found is connected to all nodes in candidates
        boolean end = false;
        int edgecounter;
        for(int i=0; i<already_found.size(); i++){
            Integer found = already_found.get(i);
            edgecounter = 0;
            for (int j=0; j<candidates.size(); j++){
                Integer candidate = candidates.get(j);
                if (graph.isNeighbor(found, candidate)) {
                    edgecounter++;
                } // of if
            } // of for
            if (edgecounter == candidates.size()) {
                end = true;
            }
        } // of for
        return end;
    }

    private boolean sonComunidad(HashSet<Integer> cliqueA, HashSet<Integer> cliqueB){
        HashSet<Integer> cliquePeque = new HashSet<>();
        HashSet<Integer> cliqueGrande = new HashSet<>();
        if (cliqueA.size() < cliqueB.size()){
            cliquePeque = cliqueA;
            cliqueGrande = cliqueB;
        }
        else {
            cliquePeque = cliqueB;
            cliqueGrande = cliqueA;
        }

            int num_coincidencias = 0;
            for(Integer u : cliquePeque){
                for (Integer w : cliqueGrande){
                    if(u == w) num_coincidencias++;
                }
            }
            if(num_coincidencias == (cliquePeque.size() - 1 )) return true;
            else return false;

    }

    private HashSet<HashSet<Integer>> comunidades(List<HashSet<Integer>> cliques){

        //Saber a que comunidad pertenece cada clique
        int numeros_cliques = cliques.size();
        int[] comunidadesCliques = new int[numeros_cliques];
        Arrays.fill(comunidadesCliques, -1);
        boolean comunidad[] = new boolean[numeros_cliques];
        Arrays.fill(comunidad, Boolean.FALSE);
        HashSet<HashSet<Integer>> comunidades = new HashSet<HashSet<Integer>>();
        HashSet<Integer> comunidadTemporal;
        int j=0;
        int com = 1;
        for(HashSet<Integer> obj : cliques){
            //SI clique no pertenece a una comunidad
            if (comunidadesCliques[j]==-1)  {
                comunidadesCliques[j]=com;
                comunidad[j]=true;
                ++com;
            }
            int k=0;
            for (HashSet<Integer> o : cliques) {
                if(comunidad[k]==false) {
                    boolean c = sonComunidad(obj, o);
                    if(c){
                        comunidad[k]=true;
                        comunidadesCliques[k]=comunidadesCliques[j];
                    }
                }
                ++k;
            }
            ++j;
        }

        //sabiendo a que comunidad pertenece cada clique creame el conjunto de comunidades
        for(int c=1; c < com; ++c){
            HashSet<Integer> temporal;
            temporal = new HashSet<>();
            int clic = 0;
            for(HashSet<Integer> clique : cliques){
                if(comunidadesCliques[clic]==c){
                    if(temporal.isEmpty()) temporal=clique;
                    else {
                        for(Integer u : clique){
                            temporal.add(u);
                        }}
                }
                ++clic;
            }
            comunidades.add(temporal);
        }

        return comunidades;
    }

    private List<HashSet<Integer>> getCliquesOrder()
    {
        //tenemos en cliques todos los cliques
        int maximum = 0;
        List<HashSet<Integer>> order_cliques = new ArrayList<HashSet<Integer>>();
        for (HashSet<Integer> clique : cliques) {
            if (maximum < clique.size()) {
                maximum = clique.size();
            }
        }
        for(int i = maximum; i>=0; --i) {
            for (HashSet<Integer> clique : cliques) {
                if (i == clique.size()) {
                    order_cliques.add(clique);
                }
            }
        }
        return order_cliques;
    }

    public HashSet<HashSet<Integer>> executa(Graf G, Integer percentatge){

        //ME encuentra todos los cliques que existen en el grafo
        getAllMaximalCliques();

        //Me ordena los cliques de mayor a menor(K mas alta)
        List<HashSet<Integer>> cliquesOrder = getCliquesOrder();

        //ENSEÑAR CLIQUES YA ordenados
        int i = 1;
        for(HashSet<Integer> obj : cliquesOrder){

            System.out.println("clique " + obj.size());
            for(Integer o : obj){
                System.out.println(" " + o);
            }
            ++i;
        }

        //ME encuentra las comunidades a partir de los cliques
        HashSet<HashSet<Integer>> comunidades = comunidades(cliquesOrder);

        return comunidades;
    }

}
